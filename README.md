### About Advent of Code
> Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as a speed contest, interview prep, company training, university coursework, practice problems, or to challenge each other.
from https://adventofcode.com/2020/about

### About this repo 
Python and puzzles: what a great combination! As much fun as it is to solve the daily problems, it's been almost more valuable to read other people's solutions. 2020 is the first year I have participated. I plan to work on previous years' problems at some point after this season is over.

### About me
I started coding in Python in December 2019 as I contemplated a career change. It was a good move: a couple of months later, I had the opportunity to switch roles within my project from customer support to technical support. As I continued to study, I focused on applying Python to data science problems. A few months ago, I started work half-time on a data science project and transitioned to full time Data Scientist on December 1, 2020. 


