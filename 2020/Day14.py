def get_data(filename):
    with open(filename, 'r') as sourcefile:
        return [[val.strip() for val in line.split('=')] for line in sourcefile.readlines()]


def get_mem_place(command):
    try:
        return int(command.split('[')[1].split(']')[0])
    except:
        return -1


def get_value(command):
    if 'mem' in command[0]:
        # note we need a mutable list, not an immutable string
        return list(format(int(command[1]), '036b'))
    else:
        # the command is to mask, and for consistency should also return a list
        return list(command[1])


def binary_list_to_decimal(value):
    print('binary list to decimal', ''.join(value), int(''.join(value), base=2))
    return int(''.join(value), base=2)


def apply_mask(mask, value):
    print(''.join(mask))
    print(''.join(value))
    for i, val in enumerate(mask):
        if val != 'X':
            value[i] = val
    print(''.join(value), '\n')
    return value


data = get_data('Day14.txt')
memory = dict()
commands = [[d[0], get_value(d)] for d in data]
for c in commands:
    # print(c[0], ''.join(c[1]))
    # print('memory', get_mem_place(c[0]))
    if get_mem_place(c[0]) > 0:
        memory[get_mem_place(c[0])] = c[1]
        # print(''.join(c[1]))

[print(key, ''.join(memory[key])) for key in memory.keys()]

mask = []
sum = 0
for c in commands:
    if c[0] == 'mask':
        mask = c[1]
    else:
        value = apply_mask(mask, c[1])

for key in memory.keys():
    sum += binary_list_to_decimal(memory[key])

print(sum)


