from collections import deque


def get_data(filename):
    with open(filename, 'r') as sourcefile:
        return [line.strip() for line in sourcefile.readlines()]


def set_up_game(data):
    player2_index = [data.index(d) for d in data if 'Player' in d][1]
    p1 = deque([int(d) for d in data[1:player2_index - 1]])
    p2 = deque([int(d) for d in data[player2_index + 1:]])
    return p1, p2

def play_round(p1, p2):
    r1 = p1.popleft()
    r2 = p2.popleft()
    if r1 > r2:
        p1.extend([r1, r2])
    else:
        p2.extend([r2, r1])
    return p1, p2

def play_game(p1, p2):
    total_cards = len(p1) + len(p2)
    max_cards = max(len(p1), len(p2))
    print(p1, p2)
    while max_cards < total_cards:
        p1, p2 = play_round(p1, p2)
        max_cards = max(len(p1), len(p2))
    return p1, p2

def get_winner(p1, p2):
    if len(p1) > 0:
        return p1
    else:
        return p2

def get_score(player):
    total = 0
    i = 1
    while len(player) > 0:
        total += player.pop()*i
        i += 1
    return total

data = get_data('Day22.txt')
print(data)
p1, p2 = set_up_game(data)
p1, p2 = play_game(p1, p2)
winner = get_winner(p1, p2)
print(winner)
print(get_score(winner))

