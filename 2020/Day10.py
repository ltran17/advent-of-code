from itertools import combinations


def get_data():
    with open('Day10.txt', 'r') as datasource:
        raw = datasource.readlines()
    data = []
    for r in raw:
        data.append(int(r.strip()))
    return data


data = get_data()
data.append(0)
data = sorted(data)
data.append(data[-1] + 3)
print(data)

differences = [data[j + 1] - data[j] for j in range(len(data) - 1)]
jolt1 = differences.count(1)
jolt3 = differences.count(3)
print('1:', jolt1, '3:', jolt3, 'answer:', jolt1 * jolt3)


def check_valid_combination(adapters):
    # adapters is a sorted list of jolts. If any of the differences are > 3, this is not a valid list
    print('checking', adapters)
    differences = [adapters[j + 1] - adapters[j] for j in range(len(adapters) - 1)]
    if max(differences) > 3:
        return False
    return True


# print(check_valid_combination(data))
# combos = combinations(data,2)
# print(combos.__sizeof__())

def get_number_valid_combinations_of_length(data, length, max_val):
    combos = combinations(data, length)
    count = 0
    for c in combos:
        if (0 in c) and (max_val in c):
            count += check_valid_combination(c)
    return count


total_valid_combos = 0
# THIS IS NOT GOING TO WORK the computer will melt and my kids will be out of college by the time it's over...
# for combo_len in range(len(data)//3, len(data)):
#     total_valid_combos += get_number_valid_combinations_of_length(data, combo_len, max(data))

# NEW IDEA: find out how long the sequences of 1-Jolt differences are. Get permutations that follow the rule for each length of sequence, then multiply.
one_j_set = set()
one_js_list = []
for i in range(len(data) - 1):
    print(data[i], data[i + 1], differences[i])
    one_j_set.add(data[i])
    if differences[i] == 3:
        one_js_list.append(one_j_set)
        one_j_set = set()
for j in one_js_list:
    print(j)
"""
Here's what I calculated by hand for jolt lists:
length 1 -> 1
length 2 -> 1
length 3 -> 2
length 4 -> 4
length 5 -> 7
"""
mult = 1
for j in one_js_list:
    if len(j) == 1:
        p = 1
    elif len(j) == 2:
        p = 1
    elif len(j) == 3:
        p = 2
    elif len(j) == 4:
        p = 4
    elif len(j) == 5:
        p = 7
    mult *= p

print('number of routes is', mult)

