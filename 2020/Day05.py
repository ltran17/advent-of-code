# Found this three line solution on Twitter. So elegant!
def three_line_solution():
    seatID = lambda s: int(s.replace('F', '0').replace('B', '1').replace('L', '0').replace('R', '1'), base=2)
    ids = set(map(seatID, open('Day05.txt').read().splitlines()))
    print(max(ids), (set(range(min(ids))) - ids).pop())
"""
Test data:
BFFFBBFRRR: row 70, column 7, seat ID 567.
FFFBBBFRRR: row 14, column 7, seat ID 119.
BBFFBBFRLL: row 102, column 4, seat ID 820.

Rows: F = 0, B = 1
Columns: L = 0, R = 1
Seat ID = 8*Row + Column
What is the highest Seat ID?

Note: represent a binary value with '0b' prepended to the binary string.
        To get its integer value, cast as int.
"""


def binary_string_to_integer(bin_string: str, zero: str, one: str) -> int:
    """
    :param bin_string: string of zero and one characters
    :param zero: the character representing 0
    :param one: the character representing 1
    :return: integer value of the binary string
    """
    binary = '0b' + bin_string.replace(zero, '0').replace(one, '1')
    return int(binary, base=2)


test_data = ['BFFFBBFRRR', 'FFFBBBFRRR', 'BBFFBBFRLL']
for d in test_data:
    print(binary_string_to_integer(d[:7], zero='F', one='B'))


def get_seat_info(seat: str):
    row = binary_string_to_integer(seat[0:7], zero='F', one='B')
    col = binary_string_to_integer(seat[7:], zero='L', one='R')
    seat_id = 8 * row + col
    return row, col, seat_id


for d in test_data:
    print(get_seat_info(d))

seat_list = [get_seat_info(s) for s in test_data]
print('max seat is', max(seat_list, key=lambda s: s[2]))

with open('Day05.txt', 'r') as sourcefile:
    data = [line.strip() for line in sourcefile]

print('Real data below...')
seat_list = [get_seat_info(s) for s in data]
min_seat = min(seat_list, key=lambda s: s[2])
max_seat = max(seat_list, key=lambda s: s[2])
print('min seat is', min_seat)
print('max seat is', max_seat)

"""
--- Part Two ---

Ding! The "fasten seat belt" signs have turned on. Time to find your seat.

It's a completely full flight, so your seat should be the only missing boarding pass in your list. However, there's a catch: some of the seats at the very front and back of the plane don't exist on this aircraft, so they'll be missing from your list as well.

Your seat wasn't at the very front or back, though; the seats with IDs +1 and -1 from yours will be in your list.

What is the ID of your seat?
"""

seat_id_list = sorted([s[2] for s in seat_list])
all_seat_list = [i for i in range(85, max_seat[2] + 1)]
missing = set(all_seat_list) - set(seat_id_list)
print(missing, 'is missing')

print(three_line_solution())