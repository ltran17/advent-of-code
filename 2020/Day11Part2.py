class Seating:
    def __init__(self, seating_arrangement):
        """
        :param seating_arrangement: list of seat strings (values are empty: L, full: #, floor: .
        """
        self.seating = seating_arrangement
        self.num_rows = len(seating_arrangement)
        self.num_cols = len(seating_arrangement[0])
        self.compass = {
            'N': (0, 1),
            'NE': (1, 1),
            'E': (1, 0),
            'SE': (1, -1),
            'S': (0, -1),
            'SW': (-1, -1),
            'W': (-1, 0),
            'NW': (-1, 1)
        }

    def __str__(self):
        row_string = ''
        for row in self.seating:
            for s in row:
                row_string += s + ' '
            row_string += '\n'
        return row_string

    def __eq__(self, other):
        return self.seating == other.seating

    def __ne__(self, other):
        return self.seating != other.seating

    def is_valid_seat(self, row, col):
        return (0 <= row < self.num_rows) and (0 <= col < self.num_cols)

    def check_occupied_in_dir(self, row, col, dir):
        vec = self.compass[dir]
        mult = 1
        check_row = row + vec[0]
        check_col = col + vec[1]
        while self.is_valid_seat(check_row, check_col):
            if self.seating[check_row][check_col] == '#':
                return True
            elif self.seating[check_row][check_col] == 'L':
                return False
            mult += 1
            check_row = row + mult * vec[0]
            check_col = col + mult * vec[1]
        return False

    def get_visible_occupied(self, row, col):
        occupied = 0
        for dir in self.compass:
            occupied += self.check_occupied_in_dir(row, col, dir)
        return occupied

    def get_seat_status(self, row, col):
        return self.seating[row][col]

    def count_occupied_seats(self):
        occupied = 0
        for row in self.seating:
            occupied += row.count('#')
        return occupied

def get_data(filename):
    with open(filename, 'r') as sourcefile:
        data = [line.strip() for line in sourcefile.readlines()]
    return data


def make_next_round(seating):
    next_round = []
    for r in range(seating.num_rows):
        row_string = ''
        for c in range(seating.num_cols):
            seat_val = seating.get_seat_status(r, c)
            if (seat_val == '#') and (seating.get_visible_occupied(r, c) >= 5):
                row_string += 'L'
            elif (seat_val == 'L') and (seating.get_visible_occupied(r, c) == 0):
                row_string += '#'
            else:
                row_string += seat_val
        next_round.append((row_string))
    return next_round


seating = Seating(get_data('Day11.txt'))
round_count = 1
next_round = Seating(make_next_round(seating))
print(seating, '\n\n')
print('Round 1')
print(next_round)

while next_round != Seating(make_next_round(next_round)):
    round_count += 1
    next_round = Seating(make_next_round(next_round))
    print('\n\n')
    print('Round', round_count)
    print(next_round)

print('Occupied seats:', next_round.count_occupied_seats())