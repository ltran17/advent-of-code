def get_data(filename):
    with open(filename, 'r') as sourcefile:
        return [line.strip() for line in sourcefile.readlines()]


def show_seating(data):
    [print(d) for d in data]


def get_number_neigbor_seats(row, col, data):
    neighbor_seats = 0
    neighbor_indices = [-1, 0, 1]
    for i in neighbor_indices:
        for j in neighbor_indices:
            if (row + i, col + j) == (row, col):
                pass # don't count the seat itself
            elif 0 <= row + i < len(data) and 0 <= col + j < len(data[col]):
                # making sure the indices are in range
                if data[row + i][col + j] != '.':
                    # not counting floor seats
                    neighbor_seats += 1
    return neighbor_seats

def test_neighbor_seats(data):
    for row in range(len(data)):
        row_string = ''
        for col in range(len(data[row])):
            row_string += str(get_number_neigbor_seats(row, col, data))
        print(row_string)

def get_num_adjacent_seats_with_value(value, row, col, data):
    count = 0
    neighbor_indices = [-1, 0, 1]
    for i in neighbor_indices:
        for j in neighbor_indices:
            if (row + i, col + j) == (row, col):
                pass # don't count the seat itself
            elif (0 <= row + i < len(data)) and (0 <= col + j < len(data[0])):
                # making sure the indices are in range
                if data[row + i][col + j] == value:
                    count += 1
    # print('seats adjacent to data', row, col, data[row][col], 'with value', value, 'are', count)
    return count

def get_next_round(data):
    next_round = []
    for r in range(len(data)):
        row_string = ''
        for c in range(len(data[r])):
            #print('here we go looking at the bug', r, c, data[r][c])
            if (data[r][c] == 'L') and (get_num_adjacent_seats_with_value('#', r, c, data) == 0):
                row_string += '#'
                # print(row_string)
            elif (data[r][c] == '#') and (get_num_adjacent_seats_with_value('#', r, c, data) >= 4):
                row_string += 'L'
                # print(row_string)
            else:
                row_string += data[r][c]
                # print(row_string)
        next_round.append(row_string)
    return next_round

def count_occupied_seats(next_round):
    num_occupied = 0
    for row in next_round:
        num_occupied += row.count('#')
    return num_occupied


data = get_data('Day11.txt')
show_seating(data)
round_count = 1
next_round = get_next_round(data)
print('\n\n')
print('Round', round_count)
show_seating(next_round)

while next_round != get_next_round(next_round):
    round_count += 1
    next_round = get_next_round(next_round)
    print('\n\n')
    print('Round', round_count)
    show_seating(next_round)

print('total occupied seats are', count_occupied_seats(next_round))