class Ship:
    def __init__(self, posx, posy, dir):
        self.starting_posx = posx
        self.starting_posy = posy
        self.posx = posx
        self.posy = posy
        self.dir = dir

    def __str__(self):
        return f"({self.posx}, {self.posy}) facing {self.dir}"

    def update_direction(self, new_dir):
        self.dir = new_dir
        print(self.dir)

    def update_position(self, new_posx, new_posy):
        self.posx += new_posx
        self.posy += new_posy
        print('updated pos', self.posx, self.posy)

    def turn(self, RL, degree):
        if RL == 'L':
            degree = -1*degree
        compass_to_deg = {'E': 0, 'S': 90, 'W': 180, 'N': 270}
        deg_to_compass = {0: 'E', 90: 'S', 180: 'W', 270: 'N'}
        heading = (compass_to_deg[self.dir] + degree)%360
        self.update_direction(deg_to_compass[heading])


    def move(self, action):
        """
        Update the ship's position based on given input
        :param action: ((compass directions ESWN, forward F, turn R or L), number of moves or degrees)
        :return:
        """
        dir = action[0]
        if dir in 'RL':
            self.turn(dir, action[1])
        elif dir in 'FESWN':
            if dir == 'F': # Forward in current direction
                dir = self.dir
                print('going forward in direction', dir)
            if dir == 'E':
                self.update_position(action[1], 0)
            elif dir == 'S':
                self.update_position(0, -action[1])
            elif dir == 'W':
                self.update_position(-action[1], 0)
            elif dir == 'N':
                self.update_position(0, action[1])
        print('\t', self)


    @property
    def manhattan_distance(self):
        """
        How far the Ship is from its starting point
        :return: Taxicab distance calculation
        """
        return abs(self.starting_posx - self.posx) + abs(self.starting_posy - self.posy)

def get_data(filename):
    with open(filename, 'r') as sourcefile:
        raw = sourcefile.readlines()
    return [r.strip() for r in raw]

def parse_movement(move):
    return move[0], int(move[1:])


data = get_data('Day12.txt')
ship = Ship(0, 0, 'E')
moves = [parse_movement(d) for d in data]
for m in moves:
    print(m)
    ship.move(m)
print('distance', ship.manhattan_distance)


