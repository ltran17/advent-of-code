"""
--- Day 6: Custom Customs ---

As your flight approaches the regional airport where you'll switch to a much larger plane, customs declaration forms are distributed to the passengers.

The form asks a series of 26 yes-or-no questions marked a through z. All you need to do is identify the questions for which anyone in your group answers "yes". Since your group is just you, this doesn't take very long.

However, the person sitting next to you seems to be experiencing a language barrier and asks if you can help. For each of the people in their group, you write down the questions for which they answer "yes", one per line. For example:

abcx
abcy
abcz
In this group, there are 6 questions to which anyone answered "yes": a, b, c, x, y, and z. (Duplicate answers to the same question don't count extra; each question counts at most once.)
...
Another group asks for your help, then another, and eventually you've collected answers from every group on the plane (your puzzle input). Each group's answers are separated by a blank line, and within each group, each person's answers are on a single line.
abc

a
b
c

ab
ac

a
a
a
a

b
This list represents answers from five groups:

The first group contains one person who answered "yes" to 3 questions: a, b, and c.
The second group contains three people; combined, they answered "yes" to 3 questions: a, b, and c.
The third group contains two people; combined, they answered "yes" to 3 questions: a, b, and c.
The fourth group contains four people; combined, they answered "yes" to only 1 question, a.
The last group contains one person who answered "yes" to only 1 question, b.
In this example, the sum of these counts is 3 + 3 + 3 + 1 + 1 = 11.

For each group, count the number of questions to which anyone answered "yes". What is the sum of those counts?
"""


def get_data() -> list:
    with open('Day06.txt', 'r') as sourcefile:
        raw = sourcefile.readlines()
    data = [line.strip() for line in raw]
    return data


def parse_groups(data: list) -> list:
    """
    :param data: list of individual answer, groups separated by ''
    :return: list of groups, each element is a list of individual answers
    """
    groups = []
    group = []
    for answers in data:
        if answers != '':
            group.append(answers)
        else:
            groups.append(group)
            group = []
    groups.append(group)  # Was off by one ... come back to this to figure out why and improve
    return groups

def get_group_question_counts(group: list) -> int:
    """
    :param group: list of individual responses
    :return: number of unique responses for the group
    """
    answer_str = ''
    for a in group:
        answer_str += a
    return len(set(answer_str))

import string

def create_frequency_table(group: list) -> dict:
    """
    :param group: list of individual responses
    :return: frequency table of answers
    """
    #  Part of me very much wants to use pandas and value_counts()
    #  but I feel like I should stick with basic data structures
    counts = dict(zip(string.ascii_lowercase, [0 for i in range(26)]))
    for individual in group:
        for answer in individual:
            counts[answer] += 1
    return counts


def count_common_answers(group: list) -> int:
    """
    :param group: list of individual responses
    :return: number of answers individuals have in common
    """
    group_size = len(group)
    counts = create_frequency_table(group)
    common_answers = 0
    for key in counts:
        if counts[key] == group_size:
            common_answers += 1
    return common_answers


data = get_data()
groups = parse_groups(data)
sum = 0
common = 0
for g in groups:
    sum += get_group_question_counts(g)
    common += count_common_answers(g)
print(common)

"""
Code below is NOT MY OWN but an elegant solution from Twitter @Brotherluii
"""
with open('Day06.txt') as file:
    groups = [group.replace('\n', ' ').split()
              for group in ''.join(file.read()).split('\n\n')]

total1 = total2 = 0
for answer in groups:
    yes1 = set()
    yes2 = set(answer[0])
    for char in answer:
        yes1 |= set(char)  # Part 1
        yes2 &= set(char)  # Part 2
    total1 += len(yes1)
    total2 += len(yes2)
print(total1, total2)
