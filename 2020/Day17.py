class Layer:
    def __init__(self):
        """
        Initializes 2D dictionary
        """
        self.compass = {
            'N': (0, 1),
            'NE': (1, 1),
            'E': (1, 0),
            'SE': (1, -1),
            'S': (0, -1),
            'SW': (-1, -1),
            'W': (-1, 0),
            'NW': (-1, 1),
            'C': (0, 0)
        }

class Cube(x, y, z):
    '''
    Initializes 3D Dictionary
    '''


