def get_data(filename):
    with open(filename, 'r') as sourcefile:
        raw = [line for line in sourcefile.readlines()]
    # class, row, seat
    return [[val.strip() for val in line.split(':')] for line in raw]


def get_missing_values(data):
    value_set = set()
    for d in data:
        if (len(d) == 2) and ('or' in d[1]):
            ranges = [[v.strip() for v in val.split('-')] for val in d[1].split('or')]
            for r in ranges:
                value_set.update([i for i in range(int(r[0]), int(r[1])+1)])
            print(ranges)
    missing = set([i for i in range(min(value_set), max(value_set)+1)]) - value_set
    return missing, min(value_set), max(value_set)

def get_nearby_tickets(data):
    idx = data.index(['nearby tickets', ''])
    return data[idx + 1:]

data = get_data('Day16test.txt')
missing, min_val, max_val = get_missing_values(data) #values less than min and greater than max are invalid, too
nearby = get_nearby_tickets(data)
sum = 0

remove_tix_idx = set()
for ticket in nearby:
    for val in [int(t) for t in ticket[0].split(',')]:
        if (val in missing) or (val < min_val) or (val > max_val):
            sum += val
            remove_tix_idx.add(nearby.index(ticket))
print(sum)
print(remove_tix_idx)