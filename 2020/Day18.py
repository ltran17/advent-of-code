def get_data(filename):
    with open(filename, 'r') as sourcefile:
        return [line.strip() for line in sourcefile.readlines()]


def do_operation(x, y, op):
    if op == '+':
        return x + y
    if op == '*':
        return x*y
    return -1  # this indicates an error

def operate_string(val_ops):
    print('val_ops', val_ops)
    left = val_ops[0]
    if left == '(':
        operate_string(val_ops[1:])
    operation = val_ops[2]
    right = val_ops[4]
    print(left, operation, right)
    if right == '(':
        right = operate_string(val_ops[5:])
    return do_operation(int(left), int(right), operation)

# data = get_data('Day18test.txt')
# for d in data:
#     print(d)
#     print(operate_string(d))

print(operate_string('2 * (4 * 3) + 5'))