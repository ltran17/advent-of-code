"""
--- Day 4: Passport Processing ---

You arrive at the airport only to realize that you grabbed your North Pole Credentials instead of your passport. While these documents are extremely similar, North Pole Credentials aren't issued by a country and therefore aren't actually valid documentation for travel in most of the world.

It seems like you're not the only one having problems, though; a very long line has formed for the automatic passport scanners, and the delay could upset your travel itinerary.

Due to some questionable network security, you realize you might be able to solve both of these problems at the same time.

The automatic passport scanners are slow because they're having trouble detecting which passports have all required fields. The expected fields are as follows:

byr (Birth Year)
iyr (Issue Year)
eyr (Expiration Year)
hgt (Height)
hcl (Hair Color)
ecl (Eye Color)
pid (Passport ID)
cid (Country ID)
Passport data is validated in batch files (your puzzle input). Each passport is represented as a sequence of key:value pairs separated by spaces or newlines. Passports are separated by blank lines.

Here is an example batch file containing four passports:

ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in
The first passport is valid - all eight fields are present. The second passport is invalid - it is missing hgt (the Height field).

The third passport is interesting; the only missing field is cid, so it looks like data from North Pole Credentials, not a passport at all! Surely, nobody would mind if you made the system temporarily ignore missing cid fields. Treat this "passport" as valid.

The fourth passport is missing two fields, cid and byr. Missing cid is fine, but missing any other field is not, so this passport is invalid.

According to the above rules, your improved system would report 2 valid passports.

Count the number of valid passports - those that have all required fields. Treat cid as optional. In your batch file, how many passports are valid?
"""


def get_data():
    with open('Day04.txt', 'r') as datasource:
        raw = datasource.readlines()
    data = []
    for r in raw:
        data.append(r.strip())
    return data


def get_passport_list(data):
    passport_list = []
    passport = dict() # or passport = {}
    for d in data:
        if len(d) > 0:
            d_list = d.split()
            for entry in d_list:
                e = entry.split(':')
                # alternate and very nice:
                # k, v = entry.split(':')
                # passport[k] = v
                passport[e[0]] = e[1]
        else:
            passport_list.append(passport)
            passport = dict()
    passport_list.append(passport)
    return passport_list


def check_passport(passport):
    if len(passport) == 8:
        return True
    elif (len(passport) == 7) and ('cid' not in passport.keys()):
        return True
    else:
        return False


data = get_data()
passports = get_passport_list(data)

valid_count = 0
for passport in passports:
    valid_count += check_passport(passport)
print('there are', valid_count, 'of', len(passports))


"""
--- Part Two ---

The line is moving more quickly now, but you overhear airport security talking about how passports with invalid data are getting through. Better add some data validation, quick!

You can continue to ignore the cid field, but each other field has strict rules about what values are valid for automatic validation:

byr (Birth Year) - four digits; at least 1920 and at most 2002.
iyr (Issue Year) - four digits; at least 2010 and at most 2020.
eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
hgt (Height) - a number followed by either cm or in:
If cm, the number must be at least 150 and at most 193.
If in, the number must be at least 59 and at most 76.
hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
pid (Passport ID) - a nine-digit number, including leading zeroes.
cid (Country ID) - ignored, missing or not.
Your job is to count the passports where all required fields are both present and valid according to the above rules.
"""

def check_rules(passport):
    if not ('1920' <= passport['byr'] <= '2002'):
        return False
    if not ('2010' <= passport['iyr'] <= '2020'):
        return False
    if not ('2020' <= passport['eyr'] <= '2030'):
        return False
    if not ('cm' in passport['hgt'] or 'in' in passport['hgt']):
        return False
    value = passport['hgt'][:-2]
    unit = passport['hgt'][-2:]
    if not (
            ((unit == 'cm') and ('150' <= value <= '193')) or
            ((unit == 'in') and ('59' <= value <= '76'))):
        return False
    if (len(passport['hcl']) != 7) or (passport['hcl'][0] != '#'):
        return False
    for c in passport['hcl'][1:]:
        if c not in '0123456789abcdef':
            return False
    # BETTER:
    # if 'hcl' in passport:
    #   hcl = passport['hcl']
    #   if hcl[0] != '#' or any([c not in '0123456789abcdef' for c in hcl[1:]]):
    #       return False
    # else:
    #   return False
    if passport['ecl'] not in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
        return False
    if len(passport['pid']) != 9:
        return False
    for d in passport['pid']:
        if d not in '0123456789':
            return False
    # BETTER
    # if any([c not in '0123456789' for c in passport['pid']):
    #   return False

    return True

rule_check_sum = 0
for passport in passports:
    if check_passport(passport):
        rule_check_sum += check_rules(passport)

print(rule_check_sum)