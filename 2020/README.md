TIL so far :snake:
* (Day 1) :star: :star: Someone used ^ as an XOR operator. I hadn't seen this before and I think it is clever.
* (Day 2) :star: :star: Seeing a lot of people processing data as they read it in. Is this good practice? I feel like I'm alone in reading in the raw data and processing it later.
* (Day 3) :star: :star: Having a math background made this a quick solve. Hello to my old friends `dy/dx` and modulo arithmetic.
* (Day 4) :star: :star: A lot of people solved this with regex. I solved with Python's slicing.
* (Day 5) :star: :star: How to convert a binary string to an integer. `int(binary_string, base=2)`. Where I used a series of `str.replace()` functions, someone else used `str.maketrans('FBLR', '0101')` and I like that solution.
* (Day 6) :star: :star: Created a frequency table with a dictionary...initialized by zipping together lower case alphabet with zeros. I'm sure there's a better way. Reading other people's code: `|=` is union of sets, `&=` is intersection :mind_blown:
* (Day 7) :star: I really wanted to implement this with NetworkX because I haven't used it for a few months...and quickly realized how much I had forgotten. Recursion got me down...just one star so far, must revisit. Have not yet had a chance to review other people's solutions.
* (Day 8) :star: I seem to be writing a low level code interpreter with a high level language at a poor level of skill. One star so far. Will catch up on the weekend.
* (Day 9) :star: :star: Must remember to convert string data to integer before driving myself crazy. ALSO itertools might come in handy here...look into sliding windows algorithms...and :mind_blown: (again) people are solving with SQL -- "It's Turing complete!" they tell me.
* (Day 10) :star: :star: Part 1: So it's easy enough to check if a combination is valid but who has time to check all the possible combinations? Gotta get clever. I attempted brute force solution but that was a mistake. Part 2: I got over 32 trillion distinct solutions...thank goodness I only had to count them all and not calculate them. I'm please with my algorithm but displeased with my code.
* (Day 11): :star: :star: Figured out Part 2 and used OOP. For the first time ever, I had reason to implement the `__ne__` method for a class. I earned that second star! [Note from Part 1: STUPID STINK BUG of my own creation. I spent way too much time on Part 1. I don't know if I have the stomach for part 2.]
* (Day 12): :star: Reminds me of Logo's Turtle. :triangle: :turtle: Would be fun to learn some graphics with this one. Not sure if I will spend time on Part 2. Need to catch up on previous days.
* (Day 13): :star: Part 1 was a straightforward solve for me. Part 2...I spent a lot of time coming up with my own algorithm. At some point I gave in and looked up solutions. I'm still working on coding up the Chinese Remainder Theorem.
* (Day 14): :star: Bitmasks! I am so certain there is a better way to solve this. It feels very strange (as on Day 8) to use such a high level language to solve problems on the bit level.
* (Day 15): Spent the evening zooming with friends...will return to this on the weekend.
* (Day 16): :star: Parsing data files provides lots of opportunities to use generators, string functions, and list slicing. Part 1 easy enough to solve with sets.

General observation: I'm admiring how naturally people are using unit testing with the given example data. I tried to do it on Day 5 but the code just looks a little messier. But whatever, I'm learning.
