
def get_data(filename):
    with open(filename, 'r') as sourcefile:
        data = [line.strip() for line in sourcefile]
    return data


def part_1():
    data = get_data('Day13.txt')
    timestamp = int(data[0])
    buslist = []
    for d in data[1].split(','):
        try:
            buslist.append(int(d))
        except:
            pass
    next_bus = [((timestamp // b) + 1) * b for b in buslist]
    wait_time = [b - timestamp for b in next_bus]
    take_bus = buslist[wait_time.index(min(wait_time))]
    print('Take bus', take_bus)
    # What is the ID of the earliest bus you can take to the airport multiplied by the number of minutes you'll need to
    # wait for that bus?
    answer = take_bus * min(wait_time)
    print(answer)


def lcm(x, y):
    max_val = max([x, y])
    i = 1
    while i * max_val % y != 0:
        i += 1
    print('LCM is', i * max_val)
    return i * max_val


def check_bus(bus_number, bus_index, start_time):
    print(bus_number, bus_index, start_time, (start_time + bus_index) % bus_number)
    return (start_time + bus_index) % bus_number == 0


def get_check_sum(buses, starttime):
    checksum = 0
    for bus in buses:
        busnumber = bus[0]
        index = bus[1]
        #val = (starttime+index)%busnumber
        val = starttime%busnumber
        print(busnumber, index, val)
        if (val + index)%busnumber == 0:
        #if val == 0:
            checksum += 1
    return checksum


def part_2():
    data = get_data('Day13test.txt')
    timestamp = int(data[0])
    buslist = data[1].split(',')
    print(timestamp)
    print(buslist)
    first = lcm(timestamp, int(buslist[0]))
    buses = []
    for b in buslist:
        try:
            buses.append((int(b), buslist.index(b)))
        except:
            pass
    print(buses)
    remainders = [(b[0]-b[1], b[0]) if b[1] != 0 else (b[1], b[0]) for b in buses]
    remainders = sorted(remainders, key=lambda r: r[1], reverse=True)
    print(remainders)
    # solution is in sequence a1 + 0n1, a1 + 1n1, a1 + 2n1, a + 3n1, ...
    # 55, 55 + 59, 55 + 59*2, 55 + 59*3, ...
    a, n = remainders[0]
    for multiple in range(1000):
        value = a + multiple*n
        #print(a, n, value)
        a1, n1 = remainders[1]
        a2, n2 = remainders[2]
        for nultiple in range(1000):
            #print('\t', value%n1, '=', n2)
            if value%n1 == n2:
                print(True, 'nultiple', nultiple)
                print('\t', 'value', value, 'n1', n1, value % n1, '=', n2)
                print('a', a, 'multiple', multiple, 'n', n)








part_2()
#
# num_buses = 0
# for b in buslist:
#     if b != 'x':
#         num_buses += 1
# print(num_buses, 'buses')
# check_sum = 0
# mult = 0
# newstart = start
# while check_sum < num_buses and newstart < 1068790:
#     newstart = mult * timestamp
#     print(newstart, mult, timestamp, mult * timestamp)
#     for bus in buslist:
#         try:
#             b = int(bus)
#             index = buslist.index(bus)
#             mod = newstart % b
#             print(newstart, b, mod, index, (mod + index) % b == 0)
#             if (mod + index) % b == 0:
#                 check_sum += 1
#         except:
#             pass
#     mult += 1
#     check_sum = 0