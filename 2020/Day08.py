def get_data():
    with open('Day08.txt', 'r') as datasource:
        raw = datasource.readlines()
    data = []
    for r in raw:
        data.append(r.strip())
    return data

data = get_data()
commands = [[command, int(val), 0] for command, val in [d.split() for d in data]]
# my first double-nested generator. I don't love it but it does save space.

indices = [i for i in range(len(commands))]
command_list = dict(zip(indices, commands))
print(command_list)
index = 0
accumulator = 0

def run_commands(command_list):
    accumulator = 0
    index = 0
    while command_list[index][2] < 2:
        prev_index = index
        if command_list[index][0] == 'acc':
            accumulator += command_list[index][1]
            index += 1
        elif command_list[index][0] == 'jmp':
            index += command_list[index][1]
        elif command_list[index][0] == 'nop':
            index += 1
        command_list[index][2] += 1
        print(command_list[index], accumulator, index)
    return prev_index

bad_line = run_commands(command_list)
# reset
new_command_list = dict(zip(indices, commands))
if new_command_list[bad_line][0] == 'nop':
    new_command_list[bad_line][0] = 'jmp'
else:
    new_command_list[bad_line][0] = 'nop'
run_commands(new_command_list)

