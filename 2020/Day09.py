def get_data():
    with open('Day09.txt', 'r') as datasource:
        raw = datasource.readlines()
    data = []
    for r in raw:
        data.append(int(r.strip()))
    return data


def check_valid(x, preamble):
    for i in preamble:
        for j in preamble:
            if (i != j) and (i + j == x):
                print(x,'=', i,'+', j)
                return True
    return False


data = get_data()
preamble_length = 25
valid = True
increment = 0
for x in data[preamble_length:]:
    valid = check_valid(x, data[increment: preamble_length+increment])
    increment += 1
    if valid == False:
        invalid_number = x
        index = preamble_length+increment-1
        break
print('invalid number is', invalid_number, 'at index', index)

def find_weakness_range(invalid_number, value_list):
    # go through the value_list and find a range of contiguous numbers that sum to the invalid number

    start_index = 0
    contig_length = 2
    while contig_length < len(value_list):
        for i in range(start_index, len(value_list)-contig_length+1):
            total = sum(value_list[i:contig_length])
            if total == invalid_number:
                print(total, '= sum of ', value_list[i:contig_length])
                return value_list[i:contig_length]
        print('not found with contiguous sum length', contig_length)
        contig_length += 1

sum_to_invalid = find_weakness_range(invalid_number, data)
print('min', min(sum_to_invalid), '+ max', max(sum_to_invalid), '=', min(sum_to_invalid)+max(sum_to_invalid))


