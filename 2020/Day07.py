import networkx as nx
import re


def get_data() -> list:
    with open('Day07test.txt', 'r') as sourcefile:
        raw = sourcefile.readlines()
    data = [line.strip() for line in raw]
    return data


def get_node_connections(rule: str):
    """
    Here's me, probably doing this the hard way
    :param rule: Parent node contains list of weighted children nodes
    :return: list of tuples of (parent node, child node, edge weight)
    """
    parent, children = rule.split('contain')
    pattern = r'(bags?[.,]?)'
    parent = re.sub(pattern, '', parent).strip()
    children = re.sub(pattern, 'bag', children)
    child_list = children.split('bag')
    parent_child_list = []
    for c in child_list:
        if c != '' and 'no other' not in c:
            weight = re.findall(r'(\d+)', c)[0]
            child_name = c.split(weight)[1].strip()
            parent_child_list.append((parent, child_name, int(weight)))
    return parent_child_list


def find_predecessors(bag, predecessors, level):
    print('finding predecessors of', bag)
    bag = bag.split('bags')[0].strip()
    try:
        pred_list = DG.predecessors(bag)
        for parent in pred_list:
            predecessors.append(parent)
            find_predecessors(parent.strip(), predecessors, level=level + 1)
            print(len(set(predecessors)))
    except:
        return predecessors


def find_children_totals(bag, successors, total, level):
    print('finding total children of', bag)
    bag = bag.split('bags')[0].strip()
    succ_list = DG.successors(bag)
    print(bag)
    for child in succ_list:
        weight = DG.get_edge_data(bag, child)['weight']
        print('weight between', bag, child, 'is', weight)
        successors.append(child)
        total += weight
        print(total, level)
        try:
            total += total + weight*find_children_totals(child.strip(), successors, total, level=level + 1)
            print(total)
        except:
            return total


data = get_data()
node_info = []
for rule in data:
    node_info.append(get_node_connections(rule))
print(node_info)

DG = nx.DiGraph()
for node_list in node_info:
    for node in node_list:
        DG.add_edge(node[0], node[1], weight=node[2])

print(DG.nodes)

# predecessors = find_predecessors('shiny gold bags ', predecessors=[], level=0)
# print(predecessors)

print(find_children_totals('shiny gold bags ', successors=[], total=0, level=0))
