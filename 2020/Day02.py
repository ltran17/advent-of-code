'''
--- Day 2: Password Philosophy ---

Your flight departs in a few days from the coastal airport; the easiest way down to the coast from here is via toboggan.

The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day. "Something's wrong with our computers; we can't log in!" You ask if you can take a look.

Their password database seems to be a little corrupted: some of the passwords wouldn't have been allowed by the Official Toboggan Corporate Policy that was in effect when they were chosen.

To try to debug the problem, they have created a list (your puzzle input) of passwords (according to the corrupted database) and the corporate policy when that password was set.

For example, suppose you have the following list:

1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
Each line gives the password policy and then the password. The password policy indicates the lowest and highest number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password must contain a at least 1 time and at most 3 times.

In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no instances of b, but needs at least 1. The first and third passwords are valid: they contain one a or nine c, both within the limits of their respective policies.

How many passwords are valid according to their policies?
'''
with open('Day02.txt', 'r') as filename:
    raw = filename.readlines()

data = [i.strip() for i in raw]
rules = [i.split(': ') for i in data]

def check_rule(rule: object) -> object:
    '''
    :param rule: list of splits: [0] has the rule, [1] is the password to check
    :return: int
    '''
    freq, val = rule[0].split(' ')
    minfreq = int(freq.split('-')[0])
    maxfreq = int(freq.split('-')[1])
    password = rule[1]
    count_val_in_pass = password.count(val)
    #print(minfreq, maxfreq, val, password, count_val_in_pass)
    if minfreq <= count_val_in_pass <= maxfreq:
        return 1
    else:
        return 0

check_pass = 0
for r in rules:
    check_pass += check_rule(r)

print('total good passwords:', check_pass)

'''

Your puzzle answer was 564.

The first half of this puzzle is complete! It provides one gold star: *

--- Part Two ---

While it appears you validated the passwords correctly, they don't seem to be what the Official Toboggan Corporate Authentication System is expecting.

The shopkeeper suddenly realizes that he just accidentally explained the password policy rules from his old job at the sled rental place down the street! The Official Toboggan Corporate Policy actually works a little differently.

Each policy actually describes two positions in the password, where 1 means the first character, 2 means the second character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) Exactly one of these positions must contain the given letter. Other occurrences of the letter are irrelevant for the purposes of policy enforcement.

Given the same example list from above:

1-3 a: abcde is valid: position 1 contains a and position 3 does not.
1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.
How many passwords are valid according to the new interpretation of the policies?
'''

def check_rule2(rule):
    '''
    :param rule: list of splits: [0] has the rule, [1] is the password to check
    :return: int
    '''
    freq, val = rule[0].split(' ')
    index1 = int(freq.split('-')[0]) - 1
    index2 = int(freq.split('-')[1]) - 1
    password = rule[1]
    count = 0
    if password[index1] == val:
        count += 1
    if password[index2] == val:
        count += 1
    #print(rule, index1, password[index1], index2, password[index2], password)
    if count == 1:
        return 1
    else:
        return 0

check_pass2 = 0
for rule in rules:
    check_pass2 += check_rule2(rule)

print('total check pass 2:', check_pass2)